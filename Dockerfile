FROM ruby:3.2.1
# Ubuntu
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client postgresql-server-dev-all
RUN mkdir /myapp
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install --deployment --without development test
COPY . /myapp
# Set production environment
ENV RAILS_ENV production
# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 80
# Start the main process.
WORKDIR /myapp
# Assets, to fix missing secret key issue during building
RUN SECRET_KEY_BASE=dumb bundle exec rails assets:precompile
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
