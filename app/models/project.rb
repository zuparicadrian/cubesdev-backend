# == Schema Information
#
# Table name: projects
#
#  id           :bigint           not null, primary key
#  image        :string
#  title        :string
#  description  :text
#  category     :string
#  site_url     :string
#  technologies :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Project < ApplicationRecord
    mount_uploader :image, ImageUploader

    validates :title, presence: true, uniqueness: true, length: { minimum: 3 }
    validates :description, presence: true, length: { minimum: 10 }
    validates :category, :site_url, :technologies, presence: true
    
end