class Api::NewslettersController < ApplicationController
    skip_before_action :verify_authenticity_token

    def create
        newsletter = Newsletter.new(newsletter_params)
        if newsletter.save
            render json: { message: 'Uspješno ste se pretplatili na newsletter' }, status: :ok
        else
            render json: { errors: newsletter.errors.full_messages.join(',') }, status: 422
        end

    end

    private

    def newsletter_params
        params.require(:newsletter).permit(:email)
    end

end
