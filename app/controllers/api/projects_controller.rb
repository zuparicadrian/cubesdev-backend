class Api::ProjectsController < ApplicationController

    def index
        @projects = Project.all

        respond_to do |format|
            format.json
        end
    end
end
