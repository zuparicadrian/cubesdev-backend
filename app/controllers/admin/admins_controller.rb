class Admin::AdminsController < Admin::BaseController
    before_action :authenticate_superadmin!, only: [:create, :update, :destroy]

    def index
        @admins = Admin.all
        if current_admin.is_superadmin
            @admin = params[:admin_id].nil? ? Admin.new : Admin.find(params[:admin_id])
        end
    end

    def create
        @admin = Admin.new(admin_params)
        respond_to do |format|
            if @admin.save
                format.html{ redirect_to admin_admins_path, notice: "Administrator uspješno kreiran" }
            else
                format.html{ redirect_to admin_admins_path, alert: @admin.errors.full_messages.join(", ") }
            end
        end
    end

    def update
        @admin = Admin.find(params[:id])
        respond_to do |format|
            if @admin.update(admin_params)
                format.html{ redirect_to admin_admins_path, notice: "Administrator uspješno uređen" }
            else
                format.html{ redirect_to admin_admins_path, alert: @admin.errors.full_messages.join(", ") }
            end
        end
    end

    def destroy
        @admin = Admin.find(params[:id])
        respond_to do |format|
            if @admin.destroy
                format.html{ redirect_to admin_admins_path, notice: "Administrator uspješno obrisan" }
            else
                format.html{ redirect_to admin_admins_path, alert: @admin.errors.full_messages.join(", ") }
            end
        end
    end

    private

    def admin_params
        params.require(:admin).permit(:email, :password, :password_confirmation, :is_superadmin, :first_name, :last_name )
    end

    def authenticate_superadmin!
        unless current_admin.is_superadmin
            redirect_to root_path
        end
    end

end
