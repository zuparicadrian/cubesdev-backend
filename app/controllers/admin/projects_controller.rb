class Admin::ProjectsController < Admin::BaseController

    def index
        @projects = Project.all
        @project = params[:project_id].nil? ? Project.new : Project.find(params[:project_id])
    end

    def create
        @project = Project.new(project_params)
        respond_to do |format|
            if @project.save
                format.html{ redirect_to admin_projects_path, notice: "Projekt uspješno kreiran" }
            else
                format.html{ redirect_to admin_projects_path, alert: @project.errors.full_messages.join(", ") }
            end
        end
    end

    def update
        @project = Project.find(params[:id])
        respond_to do |format|
            if @project.update(project_params)
                format.html{ redirect_to admin_projects_path, notice: "Projekt uspješno uređen" }
            else
                format.html{ redirect_to admin_projects_path, alert: @project.errors.full_messages.join(", ") }
            end
        end
    end

    def destroy
        @project = Project.find(params[:id])
        respond_to do |format|
            if @project.destroy
                format.html{ redirect_to admin_projects_path, notice: "Projekt uspješno obrisan" }
            else
                format.html{ redirect_to admin_projects_path, alert: @project.errors.full_messages.join(", ") }
            end
        end
    end

    private

    def project_params
        params.require(:project).permit(:image, :title, :description, :category, :site_url ,:technologies)
    end
end
