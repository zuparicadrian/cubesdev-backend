site_url = request.scheme + '://' + request.host_with_port

json.projects @projects do |project|
    json.title project.title
    json.description project.description
    json.category project.category
    json.site_url project.site_url
    json.technologies project.technologies
    json.image_url site_url + project.image.url
end