class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects do |t|
      t.string :image
      t.string :title, unique: true, index: true
      t.text :description
      t.string :category
      t.string :site_url
      t.string :technologies
      t.string :github_url

      t.references :admin
      t.timestamps
    end
  end
end
