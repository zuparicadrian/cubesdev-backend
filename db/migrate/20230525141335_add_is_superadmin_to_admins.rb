class AddIsSuperadminToAdmins < ActiveRecord::Migration[7.0]
  def change
    add_column :admins, :is_superadmin, :boolean, default: false
  end
end
