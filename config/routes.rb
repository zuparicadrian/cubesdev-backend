Rails.application.routes.draw do
  devise_for :admins
  root "admin/projects#index"
  namespace :admin do
    resources :projects, except: [:show]
    resources :newsletters, only: [:index, :destroy, :new, :create]
    resources :project_contact_messages, only: [:index, :destroy]
    resources :contact_messages, only: [:index, :destroy]
    resources :admins, only: [:index, :create, :update, :destroy]
  end

  namespace :api, format: 'json' do
    get "/projects", to: "projects#index"
    post "/newsletter", to: "newsletters#create"
  end
end
